@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div id="app" class="card-body">
                    <Louncher :user="{{ auth()->user() }}"></Louncher>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
