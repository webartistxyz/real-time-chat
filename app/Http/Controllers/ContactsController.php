<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use App\Events\NewMessage;
use App\Events\MsgSeenEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ContactsController extends Controller
{
    public function get()
    {
        // get all users except the authenticated one
        $contacts = User::where('id', '!=', auth()->id())->get();

        // $contacts = DB::table('users')
        // ->leftJoin('messages', 'users.id', '=', 'messages.from')
        // ->groupBy('users.id')
        // ->select('users.*', 'messages.created_at' )
        // ->where('users.id', '!=', auth()->id())
        // ->get();

        // get a collection of items where sender_id is the user who sent us a message
        // and messages_count is the number of unread messages we have from him
        $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
            ->where('to', auth()->id())
            ->where('read', false)
            ->groupBy('from')
            ->get();
        $latestMessages = Message::latest('created_at')->get();

        // add an unread key to each contact with the count of unread messages
        $contacts = $contacts->map(function($contact) use ($unreadIds, $latestMessages) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();

            $msgFrom = $latestMessages->where('from', $contact->id)->first();
            $msgTo = $latestMessages->where('to' , $contact->id)->first();
            
            $msgFromTime = !empty($msgFrom->created_at) ? $msgFrom->created_at : null;
            $msgToTime = !empty($msgTo->created_at) ? $msgTo->created_at : null;

            $latestMessage = ($msgFromTime > $msgToTime) ? $msgFrom : $msgTo;

            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

            $contact->latest = $latestMessage ? $latestMessage->created_at->timestamp : 0;


            return $contact;
        });


        return response()->json($contacts);
    }
    public function getMessagesFor($id)
    {
        //$messages = Message::where('from', $id)->orWhere('to', $id)->get();
        // Message::where('from', $id)->where('to', auth()->id())->update(['read' => true]);
    
        // $user = User::find(Auth::id());
        // event(new MsgSeenEvent($id, $user));
        

        $messages = Message::where(function($q) use ($id) {
            $q->where('from', auth()->id());
            $q->where('to', $id);
        })->orWhere(function($q) use ($id) {
            $q->where('from', $id);
            $q->where('to', auth()->id());
        })
        ->get();
        return response()->json($messages);
    }
    public function send(Request $request)
    {
        $message = Message::create([
            'from' => auth()->id(),
            'to' => $request->contact_id,
            'text' => $request->text
        ]);

        broadcast(new NewMessage($message));

        return response()->json($message);
    }
    public function msgSeen(Request $request)
    {
        Message::where('from', $request->msg_seen)->where('to', auth()->id())->update(['read' => true]);
        
        $user = User::find(Auth::id());
        event(new MsgSeenEvent($request->msg_seen, $user));
        
        
    }
}
