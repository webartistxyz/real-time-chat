<?php

namespace App\Listeners;

use App\Events\MsgSeenEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MsgSeenListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MsgSeenEvent  $event
     * @return void
     */
    public function handle(MsgSeenEvent $event)
    {
        //
    }
}
